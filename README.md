# Todorant Linux (GTK4 & Libadwaita)

This is the Todorant client built on GTK4 and Libadwaita for Linux.

The project is currently under development. You can contribute.

# Preview

![Login page](resources/login-page.png)
![Main page](resources/main-page.png)