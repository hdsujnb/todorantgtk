package cmd

import (
	"gitlab.com/hdsujnb/todorantgtk/internal/app"
	"gitlab.com/hdsujnb/todorantgtk/internal/config"
)

func RunApplication() {
	config.LoadConfig()
	app := app.NewApplication()
	app.Run()
}
