module gitlab.com/hdsujnb/todorantgtk

go 1.18

require (
	github.com/adrg/xdg v0.4.0
	github.com/diamondburned/gotk4-adwaita/pkg v0.0.0-20220417101956-dcc3707dc307
	github.com/diamondburned/gotk4/pkg v0.0.0-20220529201008-66c7fe5d2b7c
	github.com/pelletier/go-toml v1.9.5
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/hdsujnb/todorantgtk/pkg/todorantapi v0.0.0-00010101000000-000000000000
)

require (
	github.com/google/go-querystring v1.1.0 // indirect
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20220617031537-928513b29760 // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	golang.org/x/sys v0.0.0-20220610221304-9f5ed59c137d // indirect
)

replace gitlab.com/hdsujnb/todorantgtk/pkg/todorantapi => ./pkg/todorantapi
