package app

import (
	"os"

	"github.com/diamondburned/gotk4-adwaita/pkg/adw"
	"gitlab.com/hdsujnb/todorantgtk/internal/app/window"
	"gitlab.com/hdsujnb/todorantgtk/internal/config"
)

const ApplicationId = "com.hdsujnb.todorant"

type Application struct {
	App        *adw.Application
}

func NewApplication() *Application {
	adwApp := adw.NewApplication(ApplicationId, 0)

	app := Application{
		App: adwApp,
	}

	adwApp.ConnectActivate(app.ConnectActivate)

	return &app
}

func (app *Application) Run() {
	if code := app.App.Run(os.Args); code > 0 {
		os.Exit(code)
	}
}

func (app *Application) ConnectActivate() {
	if !config.GlobalConfig.IsHaveToken() {
		AuthWindow := window.NewAuthWindow(&app.App.Application)
		AuthWindow.AdwWindow.Show()
	} else {
		MainWindow := window.NewMainWindow(&app.App.Application)
		MainWindow.AdwWindow.Show()
	}
}
