package window

import (
	"github.com/diamondburned/gotk4-adwaita/pkg/adw"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/hdsujnb/todorantgtk/internal/config"
	"gitlab.com/hdsujnb/todorantgtk/pkg/todorantapi"
)

var DefaultAuthWindowSize = [2]int{300, 370}

type AuthWindow struct {
	*BaseWindow
	LoginContent *gtk.Box
	TokenEntry *gtk.Entry
	LoginWithTokenButton *gtk.Button
}

func NewAuthWindow(app *gtk.Application) *AuthWindow {
	adwWindow := adw.NewWindow()
	adwWindow.SetApplication(app)
	authWindow := AuthWindow{
		BaseWindow: &BaseWindow {
			AdwWindow: adwWindow,
			App: app,
		},
	}

	authWindow.BuildBaseWidgets()
	authWindow.BuildProperties()
	authWindow.BuildWidgets()

	return &authWindow
}

func (window *AuthWindow) BuildProperties() {
	window.AdwWindow.SetDefaultSize(DefaultAuthWindowSize[0], DefaultAuthWindowSize[1])
	window.HeaderTitle.SetTitle("Authorization")
}

func (window *AuthWindow) BuildWidgets() {
	window.LoginContent = gtk.NewBox(gtk.OrientationVertical, 10)
	window.LoginContent.SetVExpand(true)
	window.Content.Append(window.LoginContent)

	window.LoginContent.SetHAlign(gtk.AlignCenter)
	window.LoginContent.SetVAlign(gtk.AlignCenter)

	window.TokenEntry = gtk.NewEntry()
	window.LoginContent.Append(window.TokenEntry)

	window.TokenEntry.SetPlaceholderText("Enter token")

	window.LoginWithTokenButton = gtk.NewButton()
	window.LoginContent.Append(window.LoginWithTokenButton)

	window.LoginWithTokenButton.SetLabel("Auth with token")
	window.LoginWithTokenButton.ConnectClicked(func() { window.OnClickLoginWithTokenButton() })
}

func (window *AuthWindow) OnClickLoginWithTokenButton() {
	token := window.ReadTokenEntry()
	if token == "" {
		errorDialog := NewErrorDialog("Error", "Token is empty", &window.AdwWindow.Window)
		errorDialog.Show()
		return
	}

	isValidToken := window.CheckToken()
	if isValidToken {
		window.WriteTokenToConfig()
		MainWindow := NewMainWindow(window.AdwWindow.Application())
		window.AdwWindow.Destroy()
		MainWindow.AdwWindow.Show()
	}
}

func (window *AuthWindow) CheckToken() bool {
	token := window.ReadTokenEntry()
	tdapi := todorantapi.New(token)
	userData := make(chan *todorantapi.User)
	errChan := make(chan error)
	go func() {
		user, err_ := tdapi.GetUserData()
		errChan <- err_
		userData <- user
	}()

	if err := <-errChan; err != nil {
		errorDialog := NewErrorDialog("Server Error", err.Error(), &window.AdwWindow.Window)
		errorDialog.Show()
		return false
	}
	
	return true

}

func (window *AuthWindow) WriteTokenToConfig() {
	token := window.ReadTokenEntry()
	config.GlobalConfig.Account.Token = token
	config.GlobalConfig.Update()
} 

func (window *AuthWindow) ReadTokenEntry() string {
	return window.TokenEntry.Text()
}