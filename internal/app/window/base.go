package window

import (
	"github.com/diamondburned/gotk4-adwaita/pkg/adw"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
)

type BaseWindow struct {
	AdwWindow   *adw.Window
	Content     *gtk.Box
	HeaderBar   *adw.HeaderBar
	HeaderTitle *adw.WindowTitle
	App *gtk.Application
}

func (window *BaseWindow) BuildBaseWidgets() {
	content := gtk.NewBox(gtk.OrientationVertical, 0)
	window.Content = content
	window.AdwWindow.SetContent(content)

	window.buildHeader()
}

func (window *BaseWindow) buildHeader() {
	content := window.Content

	headerBar := adw.NewHeaderBar()
	headerTitle := adw.NewWindowTitle("", "")
	headerBar.SetTitleWidget(headerTitle)

	window.HeaderBar = headerBar
	window.HeaderTitle = headerTitle

	content.Append(headerBar)
}
