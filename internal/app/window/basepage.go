package window

import (
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/hdsujnb/todorantgtk/pkg/todorantapi"
)

type BasePage struct {
	TdApi *todorantapi.Todorant
	Box *gtk.Box
}

func (bp *BasePage) BuildTab(text string) *gtk.Label {
	label := gtk.NewLabel("Current")
	label.SetHExpand(true)
	return label
}

func (bp *BasePage) buildBaseWidgets() {
	box := gtk.NewBox(gtk.OrientationVertical, 0)
	bp.Box = box

	box.SetVExpand(true)
}