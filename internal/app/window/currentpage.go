package window

import (
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/hdsujnb/todorantgtk/pkg/todorantapi"
)

type CurrentPage struct {
	*BasePage
}

func NewCurrentPage(tdapi *todorantapi.Todorant) *CurrentPage {
	currentPage := CurrentPage{
		BasePage: &BasePage{
			TdApi: tdapi,
		},
	}

	currentPage.buildBaseWidgets()
	currentPage.buildBoxProperties()

	return &currentPage
}

func (cp *CurrentPage) buildBoxProperties() {
	cp.Box.SetVExpand(true)
	cp.Box.SetHAlign(gtk.AlignCenter)
	cp.Box.SetVAlign(gtk.AlignCenter)
}
