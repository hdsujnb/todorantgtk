package window

import (
	"github.com/diamondburned/gotk4-adwaita/pkg/adw"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
)

type ErrorDialog struct {
	*BaseWindow
	Parent *gtk.Window
	Name string
	Description *gtk.Label
}

var DefaultErrorDialogSize = [2]int{400, 250}

func NewErrorDialog(name string, description string, parent *gtk.Window) *ErrorDialog {
	window := adw.NewWindow()
	app := parent.Application()
	window.SetApplication(app)

	errorDialog := ErrorDialog{
		BaseWindow: &BaseWindow{
			AdwWindow: window,
			App: app,
		},
		Parent: parent,
		Name: name,
	}
	errorDialog.BuildBaseWidgets()

	window.SetTransientFor(parent)
	window.SetModal(true)
	window.SetDefaultSize(DefaultErrorDialogSize[0], DefaultErrorDialogSize[1])

	errorDialog.HeaderTitle.SetTitle(name)

	descriptionLabel := gtk.NewLabel(description)
	errorDialog.Description = descriptionLabel

	content := errorDialog.Content
	content.Append(descriptionLabel)

	return &errorDialog
}

func (d *ErrorDialog) Show() {
	d.AdwWindow.Show()
}