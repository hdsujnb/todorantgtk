package window

import (
	"github.com/diamondburned/gotk4-adwaita/pkg/adw"
	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"gitlab.com/hdsujnb/todorantgtk/internal/config"
	"gitlab.com/hdsujnb/todorantgtk/pkg/todorantapi"
)

type MainWindow struct {
	*BaseWindow
	Menu *gtk.MenuButton
	Notebook *gtk.Notebook
	TdApi *todorantapi.Todorant
	CurrentPage *CurrentPage
}


var DefaultMainWindowSize = [2]int{800, 550}

func NewMainWindow(app *gtk.Application) *MainWindow {
	window := adw.NewWindow()
	window.SetApplication(app)

	tdapi := todorantapi.New(config.GlobalConfig.Account.Token)

	mainWindow := MainWindow{
		BaseWindow: &BaseWindow{
			AdwWindow: window,
			App: app,
		},
		TdApi: &tdapi,
	}
	mainWindow.BuildBaseWidgets()
	mainWindow.buildMenuButton()
	mainWindow.buildNoteBook()
	mainWindow.buildPages()
	mainWindow.HeaderTitle.SetTitle("Todorant")
	mainWindow.AdwWindow.SetDefaultSize(DefaultMainWindowSize[0], DefaultMainWindowSize[1])

	return &mainWindow
}

func (w *MainWindow) buildMenuButton() {
	menu := gtk.NewMenuButton()
	w.Menu = menu

	menu.SetIconName("open-menu")
	w.HeaderBar.PackEnd(menu)
}

func (w *MainWindow) buildPages() {
	w.CurrentPage = NewCurrentPage(w.TdApi)
	w.Notebook.AppendPage(w.CurrentPage.Box, w.CurrentPage.BuildTab("Current"))

	// currentPage := NewCurrentPage(w.TdApi)
	// w.Notebook.AppendPage(currentPage.Box, currentPage.BuildTab("Current"))
}

func (w *MainWindow) buildNoteBook() {
	noteBook := gtk.NewNotebook()
	w.Notebook = noteBook
	w.Content.Append(noteBook)

	// page1 := gtk.NewBox(gtk.OrientationVertical, 0)
	// page1.SetVExpand(true)
	// page1.Append(gtk.NewLabel("Some current todo"))
}

func (w *MainWindow) Show() {
	w.AdwWindow.Show()
}