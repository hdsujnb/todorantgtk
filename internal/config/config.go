package config

import (
	"fmt"
	"os"

	"github.com/adrg/xdg"
	"github.com/pelletier/go-toml"
	"github.com/sirupsen/logrus"
)

var GlobalConfig Config

func LoadConfig() {
	CreateConfig()
	GlobalConfig = *ReadConfig()
}

func GetPathToConfigDirectory() string {
	return fmt.Sprintf("%s/%s", xdg.ConfigHome, "todorantgtk")
}

func GetConfigFileName() string {
	return "config.toml"
}

func GetPathToConfig() string {
	return fmt.Sprintf("%s/%s", GetPathToConfigDirectory(), GetConfigFileName())
}

func CreateConfig() {
	if _, err := os.Stat(GetPathToConfig()); os.IsNotExist(err) {
		os.Mkdir(GetPathToConfigDirectory(), 0755)
		config := Config{}

		data, err := toml.Marshal(config)
		if err != nil {
			logrus.Fatal(err)
		}

		file, err := os.Create(GetPathToConfig())
		defer file.Close().Error()
		if err != nil {
			logrus.Fatal(err)
		}

		file.Write(data)
	}
}

func (c *Config) Update() {
	data, err := toml.Marshal(c)
	fmt.Println(string(data))
	if err != nil {
		logrus.Fatal(err)
	}
	file, err := os.OpenFile(GetPathToConfig(), os.O_RDWR, 0644)
	if err != nil {
		logrus.Fatal(err)
	}
	defer file.Close()
	file.Write(data)
}

func ReadConfig() *Config {
	tree, err := toml.LoadFile(GetPathToConfig())
	if err != nil {
		logrus.Fatal(err)
	}

	var config Config
	tree.Unmarshal(&config)
	return &config
}

type Config struct {
	Account struct {
		Token string
	}
}

func (c *Config) IsHaveToken() bool {
	return c.Account.Token != ""
}