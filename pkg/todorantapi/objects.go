package todorantapi

type User struct {
	Id                 string   `json:"_id"`
	Name               string   `json:"name"`
	Email              string   `json:"email"`
	FacebookId         string   `json:"facebookId"`
	TelegramId         string   `json:"telegramId"`
	AppleSubId         string   `json:"appleSubId"`
	Token              string   `json:"token"`
	Settings           Settings `json:"settings"`
	Timezone           string   `json:"timezone"`
	TelegramZen        bool     `json:"telegramZen"`
	TelegramLanguage   string   `json:"telegramLanguage"`
	SubscriptionStatus string   `json:"subscriptionStatus"`
	SubscriptionId     string   `json:"subscriptionId"`
	AppleReceipt       string   `json:"appleReceipt"`
	GoogleReceipt      string   `json:"googleReceipt"`
	CreatedOnApple     bool     `json:"createdOnApple"`
}

type Settings struct {
	PreserveOrderByTime         bool   `json:"preserveOrderByTime"`
	RemoveCompletedFromCalendar bool   `json:"removeCompletedFromCalendar"`
	UpdatedAt                   string `json:"updatedAt"`
	StartTimeOfDay              string `json:"startTimeOfDay"`
	FirstDayOfWeek              int    `json:"firstDayOfWeek"`
}

type SettingsParams struct {
	ShowTodayOnAddTodo bool `json:"showTodayOnAddTodo"`
	FirstDayOfWeek     int  `json:"firstDayOfWeek"`
}

type Todo struct {
	Id           string `json:"_id,omitempty"`
	Text         string `json:"text"`
	Completed    bool   `json:"completed"`
	Frog         bool   `json:"frog"`
	FrogFails    int    `json:"frogFails"`
	Skipped      bool   `json:"skipped"`
	Order        int    `json:"order"`
	MonthAndYear string `json:"monthAndYear"`
	Date         string `json:"date"`
	Time         string `json:"time"`
	GoFirst      bool   `json:"goFirst"`
	Today        string `json:"today"`
	Repetitive   bool   `json:"repetitive"`
	Deleted      bool   `json:"deleted"`
	Encrypted    bool   `json:"encrypted"`
	ClientId     string `json:"clientId"`
	User         User   `json:"user"`
	CreatedAt    string `json:"createdAt"`
	UpdatedAt    string `json:"updatedAt"`
}

type GetTodoParams struct {
	Completed    bool   `url:"completed"`
	Hash         string `url:"hash"`
	QueryString  string `url:"queryString"`
	Limit        int    `url:"limit"`
	Skip         int    `url:"skip"`
	Today        string `url:"today"`
	Date         string `url:"date"`
	CalendarView bool   `url:"calendarView"`
	Time         string `url:"time"`
}

type ResponseTodos struct {
	Todos  []Todo `json:"todos"`
	Points int    `json:"points"`
}

type GetCurrentTodoParams struct {
	Date string `url:"date"`
}

type ResponseCurrentTodo struct {
	TodosCount           int  `json:"todosCount"`
	IncompleteTodosCount int  `json:"incompleteTodosCount"`
	Todo                 Todo `json:"todo"`
}
