package todorantapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/google/go-querystring/query"
)

const ApiUrl = "https://backend.todorant.com"

const RequestMethodGet = "GET"
const RequestMethodPost = "POST"
const RequestMethodPut = "PUT"
const RequestMethodDelete = "DELETE"

type ServerError struct {
	ErrorTitle   string `json:"error"`
	ErrorMessage string `json:"message"`
	ErrorCode    int
}

var TodorantGlobal *Todorant

func (e *ServerError) Error() string {
	return fmt.Sprintf("Server error: %s. Message: %s. Error code: %v", e.ErrorTitle, e.ErrorMessage, e.ErrorCode)
}

type Todorant struct {
	Token      string
	HttpClient *http.Client
}

func New(token string) Todorant {
	var httpClient = http.Client{}

	var todorant = Todorant{
		Token:      token,
		HttpClient: &httpClient,
	}

	TodorantGlobal = &todorant

	return todorant
}

// Returns json string
func (td *Todorant) Request(requestMethod string, method string, data interface{}, params interface{}) (string, error) {
	var url = fmt.Sprintf("%s/%s", ApiUrl, method)
	body, err := json.Marshal(data)
	if err != nil {
		return "", err
	}
	request, err := http.NewRequest(requestMethod, url, strings.NewReader(string(body)))
	if err != nil {
		return "", err
	}

	defer request.Body.Close()

	request.Header.Add("token", td.Token)
	switch requestMethod {
	case RequestMethodGet:
		// If content-type == application/json and request method == GET, server returns error
	default:
		request.Header.Add("Content-Type", "application/json")
	}

	paramValues, err := query.Values(params)
	if err != nil {
		return "", err
	}

	request.URL.RawQuery = paramValues.Encode()

	response, err := td.HttpClient.Do(request)
	if err != nil {
		return "", err
	}

	var responseBody, _ = ioutil.ReadAll(response.Body)
	var stringResponseBody = string(responseBody)

	switch response.StatusCode {
	case
		200,
		204:
		break
	default:
		var err ServerError
		json.Unmarshal(responseBody, &err)
		err.ErrorCode = response.StatusCode
		return "", &err
	}
	return stringResponseBody, nil
}

func (td *Todorant) GetUserData() (*User, error) {
	var data = struct {
		Token string `json:"token"`
	}{
		Token: td.Token,
	}
	response, err := td.Request(RequestMethodPost, "login/token", data, nil)
	var responseStruct User
	json.Unmarshal([]byte(response), &responseStruct)
	return &responseStruct, err
}

func (td *Todorant) SetSettings(settings SettingsParams) (string, error) {
	response, err := td.Request(RequestMethodPost, "settings", settings, nil)
	return response, err
}

func (td *Todorant) CreateTodo(todos []Todo) (string, error) {
	response, err := td.Request(RequestMethodPost, "todo", todos, nil)
	return response, err
}

func (td *Todorant) EditTodo(todo *Todo) error {
	var method = fmt.Sprintf("todo/%s", todo.Id)
	_, err := td.Request(RequestMethodPut, method, todo, struct {
		Date string `url:"date"`
		Time string `url:"time"`
	}{
		Date: todo.Date,
		Time: todo.Time,
	})

	return err
}

func (td *Todorant) GetTodos(params GetTodoParams) (*ResponseTodos, error) {
	response, err := td.Request(RequestMethodGet, "todo", nil, params)
	var responseStruct ResponseTodos
	json.Unmarshal([]byte(response), &responseStruct)
	return &responseStruct, err
}

func (td *Todorant) GetCurrentTodo(params GetCurrentTodoParams) (ResponseCurrentTodo, error) {
	response, err := td.Request(RequestMethodGet, "todo/current", nil, params)
	var responseStruct ResponseCurrentTodo
	json.Unmarshal([]byte(response), &responseStruct)
	return responseStruct, err
}

func (td *Todorant) DoneTodo(todoId string) error {
	var method = fmt.Sprintf("todo/%s/done", todoId)
	_, err := td.Request(RequestMethodPut, method, nil, nil)
	return err
}

func (td *Todorant) UnDoneTodo(todoId string) error {
	var method = fmt.Sprintf("todo/%s/undone", todoId)
	_, err := td.Request(RequestMethodPut, method, nil, nil)
	return err
}

func (td *Todorant) SkipTodo(todoId string) error {
	var method = fmt.Sprintf("todo/%s/skip", todoId)
	_, err := td.Request(RequestMethodPut, method, nil, nil)
	return err
}

func (td *Todorant) DeleteTodo(todoId string) error {
	var method = fmt.Sprintf("todo/%s", todoId)
	_, err := td.Request(RequestMethodDelete, method, nil, nil)
	return err
}
