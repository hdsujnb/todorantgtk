package util

import (
	"time"
)

func FormatDateToMonthAndYear(date time.Time) string {
	return date.Format("1-2006")
}

/*
Use this method to field 'Today'
*/
func FormatDateToToday(date time.Time) string {
	return date.Format("2006-01-02")
}

/*
Use this method to field 'Time'
*/
func FormatDateToTime(date time.Time) string {
	return date.Format("15:4")
}
